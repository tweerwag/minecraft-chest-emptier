# Copyright 2022 Timmy Weerwag
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License.

from pathlib import Path
import zlib
import struct
import re
import tempfile

import nbtlib

def extract(mca_file, output_dir):
    output_dir = Path(output_dir)
    output_dir.mkdir(exist_ok=True)
    with open(mca_file, "rb") as f:
        hdr_loc = f.read(4*1024)
        hdr_ts = f.read(4*1024)
        ts_out = output_dir / "timestamps.dat"
        ts_out.write_bytes(hdr_ts)
        for chunk in range(1024):
            location = hdr_loc[4*chunk] << 16 | hdr_loc[4*chunk + 1] << 8 | hdr_loc[4*chunk + 2]
            size_pages = hdr_loc[4*chunk + 3]
            if location == 0 and size_pages == 0:
                continue
            f.seek(location*4096)
            raw_data = f.read(size_pages*4096)
            (length, compression) = struct.unpack(">IB", raw_data[:5])
            assert(compression == 2)
            data = raw_data[5:length + 4]
            data_out = output_dir / f"chunk_{chunk}.nbt"
            data_out.write_bytes(zlib.decompress(data))

def empty_chests(nbt_file):
    nbt = nbtlib.load(nbt_file)
    for tag in nbt['']['Level']['TileEntities']:
        if 'Items' in tag.keys():
            tag['Items'] = nbtlib.List()
    nbt.save()

CHUNK_REGEX = re.compile("chunk_(\d+).nbt")
def get_chunk_number(filename):
    matches = CHUNK_REGEX.search(filename)
    return int(matches[1])

def merge(chunks_dir, mca_file):
    chunks_dir = Path(chunks_dir)
    hdr_loc = 4096 * [0]
    hdr_ts = (chunks_dir / "timestamps.dat").read_bytes()
    assert(len(hdr_ts) == 4096)
    chunks = {(get_chunk_number(str(fn)), fn) for fn in chunks_dir.glob("*.nbt")}

    with open(mca_file, "wb") as out_file:
        out_file.seek(4096)  # Skip chunks header for now
        out_file.write(hdr_ts)
        current_page = 2
        for (chunk, fn) in chunks:
            out_file.seek(current_page*4096)
            data = zlib.compress(fn.read_bytes())
            length = len(data) + 1
            out_file.write(struct.pack(">IB", length, 2))
            out_file.write(data)
            num_pages = (length + 5 + 4095) // 4096
            hdr_loc[4*chunk    ] = (current_page >> 16) & 0xff
            hdr_loc[4*chunk + 1] = (current_page >>  8) & 0xff
            hdr_loc[4*chunk + 2] =  current_page        & 0xff
            hdr_loc[4*chunk + 3] = num_pages
            current_page = current_page + num_pages
        out_file.seek(0)
        out_file.write(bytes(hdr_loc))

def process_regions(region_dir):
    for mca_file in Path(region_dir).glob("*.mca"):
        print(f"Processing {mca_file}")
        with tempfile.TemporaryDirectory() as temp_dir:
            temp_dir = Path(temp_dir)
            extract(mca_file, temp_dir)
            for nbt_file in temp_dir.glob("*.nbt"):
                empty_chests(nbt_file)
            merge(temp_dir, mca_file)
